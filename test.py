import os.path
import os
import pwd


def factorial(n):
    fac = 1
    for x in range(1, n + 1):
        fac = fac * x
    return fac


print(factorial(6))

if os.path.exists("/etc/hosts"):
    print("host file exists")
else:
    print("no hosts file")

try:
    f = open("/etc/hosts")
except FileNotFoundError:
    print("no hosts file")

maxuid = 0
for line in open("/etc/passwd"):
    split = line.split(":")
    if int(split[2]) > maxuid:
        maxuid = int(split[2])

print(maxuid)

for file in os.listdir("/root"):
    info = os.stat(file)
    print("{} : size {}".format(file, info.st_size))

for dirpath, dirnames, filenames in os.walk("/root"):
    print("Files in {} are: ".format(dirpath))
    for file in filenames:
        print("\t" + file)
    print("Directories in {} are: ".format(dirpath))
    for dir in dirnames:
        print("\t" + dir)

uidset = set()
for user in pwd.getpwall():
    uidset.add(user.pw_uid)

testdir = "/root"
for folder, dirs, files in os.walk(testdir):
    for file in files:
        path = folder + "/" + file

        if os.path.islink(path):
            print(path + " is a symlink ... skipping")
            continue

        attributes = os.stat(path)
        if attributes.st_uid not in uidset:
            print(path + " has no owner")
