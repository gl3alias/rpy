# Python Quick Start for Linux System Administrators

## Why Python

* Easy to learn and to write
* Interpreted –can be used interactively
* Powerful built-in data types
  * Strings, integers, floats
  * Lists, tuples, dictionaries, sets
* Object-oriented
  * Many pre-defined classes
  * Gives the language a “high level” feel
* Huge collection of modules
  * Many are part of the python distribution
  * Many more in your distro’s repositories
  * Huge number at Python Package Index pypi.python.org