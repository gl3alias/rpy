import re
import os
from urllib.request import urlopen
from calendar import month_abbr
import unicodedata
import sys


# 2.1
# split string with delimiters
line = 'asdf fjdk; afed, fjek,asdf, foo'
re.split(r'[;,\s]\s*', line)  # use re.split() to specify different dilimeters
# this one uses capture group, which would show delimiters too
fields = re.split(r'(;|,|\s)\s*', line)
values = fields[::2]  # to get values only
delimiters = fields[1::2] + ['']
# if we do not want to have delimiters in the first place
re.split(r'(?:,|;|\s)\s', line)

# 2.2
# string matching
filename = 'spam.txt'
filename.endswith('.txt')  # True
filename.startswith('file:')  # False

# import os
filenames = os.listdir('.')
[name for name in filenames if name.endswith(('.py', '.log'))]
any(name.endswith('.py') for name in filenames)


# from urllib.request import urlopen
def read_data(name):
    if name.startswith(('http:', 'https:', 'ftp:')):
        return urlopen(name).read()
    else:
        with open(name) as f:
            return f.read()


# 2.4
# string search and match
text = 'yeah, but no, but yeah, but no, but yeah'
text == 'yeah'
text.startswith('yeah')
text.endswith('no')
text.find('no')

text1 = '11/27/2017'
text2 = 'Nov 27,2017'
if re.match(r'\d+/\d+/\d+', text1):
    print('yes')
else:
    print('no')

datepat = re.compile(r'\d+/\d+/\d+')
if datepat.match(text1):
    print('yes')
else:
    print('no')

text = 'Today is 11/27/2017. PyCon starts on 3/13/2018'
datepat.findall(text)

datepat1 = re.compile(r'(\d+)/(\d+)/(\d+)')  # capture group
m = datepat1.match('11/27/2017')
m.group(0)
m.group(1)
m.group(2)
m.groups()
month, day, year = m.groups()
datepat1.findall(text)
for month, day, year in datepat1.findall(text):
    print('{}-{}-{}'.format(year, month, day))

# 2.5
# string search and replace
text = 'yeah, but no, but yeah, but no, but yeah'
text.replace('yeah', 'yep')

# using re.sub()
text = 'Today is 11/27/2017. PyCon starts on 3/13/2018'
re.sub(r'(\d+)/(\d+)/(\d+)', r'\3-\1-\2', text)

datapat2 = re.compile(r'(\d+)/(\d+)/(\d+)')
datapat2.sub(r'\3-\1-\2', text)


# from calendar import month_abbr
def change_date(m):
    mon_name = month_abbr[int(m.group(1))]
    return '{} {} {}'.format(m.group(2), mon_name, m.group(3))


datapat2.sub(change_date, text)

newtext, n = datapat2.subn(r'\3-\1-\2', text)

# 2.6
# ignore case when searching and replacing
text = 'UPPER PYTHON, lower python, Mixed Python'
re.findall('python', text, flags=re.IGNORECASE)
re.sub('python', 'snake', text, flags=re.IGNORECASE)


def matchcase(word):
    def replace(m):
        text = m.group()
        if text.isupper():
            return word.upper()
        elif text.islower():
            return word.lower()
        elif text[0].isupper():
            return word.capitalize()
        else:
            return word
    return replace


re.sub('python', matchcase('snake'), text, flags=re.IGNORECASE)

# 2.7
# shortest matching
str_pat = re.compile(r'\"(.*)\"')  # greedy
text1 = 'Computer says "no."'
str_pat.findall(text1)

text2 = 'Computer says "no." Phone says "Yes."'
str_pat.findall(text2)

str_pat = re.compile(r'\"(.*?)\"')  # not greedy
str_pat.findall(text2)

# 2.8
# multi-line matching
comment = re.compile(r'/\*(.*?)\*/')
text1 = '/* this is a comment */'
text2 = '''/* this is a
        multiline comment */
        '''
comment.findall(text1)
comment.findall(text2)

comment = re.compile(r'/\*((?:.|\n)*?)\*/')

# 2.11
# delete characters from string
# whitespace stripping
s = ' hello wolrd \n'
s.strip()
s.lstrip()

# character stripping
t = '---hello===='
t.lstrip('-')
t.strip('-=')

with open(filename) as f:
    lines = (line.strip() for line in f)
    for line in lines:
        print(line)

# 2.12
# clean up strings
s = 'pýtĥöñ\fis\tawesome\r\n'
remap = {
    ord('\t'): ' ',
    ord('\f'): ' ',
    ord('\r'): None
}
a = s.translate(remap)

# import unicodedata
# import sys
cmb_chrs = dict.fromkeys(c for c in range(sys.maxunicode)
                         if unicodedata.combining(chr(c)))
b = unicodedata.normalize('NFD', a)
b.translate(cmb_chrs)

# 2.13
# string adjust (align)
text = 'Hello World'
text.ljust(20)
text.rjust(20)
text.center(20)
text.rjust(20, '=')
text.center(20, '*')

format(text, '>20')  # rjust
format(text, '<20')  # ljust
format(text, '^20')  # center

'{:>10s} {:>10s}'.format('Hello', 'World')
