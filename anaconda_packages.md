# packages

click	6.7	Command line interface creation kit / BSD	
configobj	5.0.6	Config file reading, writing and validation / BSD
csvkit	0.9.1	utilities for working with CSV, the king of tabular file formats / MIT
docopt	0.6.2	Command-line interface description language / MIT
future	0.16.0	Clean single-source support for Python 3 and 2 / MIT
openpyxl	2.4.7	A Python library to read/write Excel 2010 xlsx/xlsm files / MIT
path.py	10.3.1	module wrapper for os.path / MIT
prompt_toolkit	1.0.14	library for building powerful interactive command lines in Python / BSD
psutil	5.2.2	cross-platform process and system utilities module for Python / BSD
pymysql	0.7.9	Pure-Python MySQL Driver / MIT
sh Linux Mac	1.11	full-fledged subprocess replacement for Python / MIT
