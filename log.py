import logging


def main():
    logging.basicConfig(filename='app.log', level=logging.INFO)

    hostname = 'py'
    item = 'spam'
    filename = 'data.csv'
    mode = 'r'

    logging.critical('Host %s unknown', hostname)
    logging.error('Could not find %r', item)
    logging.warning('feature is deprecated')
    logging.info('opening file %r, mode=%r', filename, mode)
    logging.debug('got here')


if __name__ == '__main__':
    main()
